# Comments
    # done using the "#" symbol
    # one-line comment: ctrl + /

# Python Syntax
print ("Hello World")

# Indentation
    # used to indicate a block of code.
        # ex.
        #   print("The indent will produce an error")

# Variables
    # terminology used for variable names are identifier
    # All identifiers should begin with (A-Z), ($) or (_).
age = 18
name = "Luke"
name1, name2, name3, name4 = "John", "Edwin", "Ken", "Rei"
print(name4)

# Data Types
# 1. Strings(str)
full_name = "John Doe"
secret_code = "Pa$$w0rd"

# 2. Numbers (int, float, complex)
num_of_days = 365
pi_approx = 3.14
complex_num = 1 + 5j

# 3. Boolean(bool)
is_learning = True
is_difficult = False

# Using Variables
print("My name is " + full_name)

# Terminal Outputs
# print() function
print("Hello World again")
print("My name is " + full_name)

    # Typecasting
    # to specify a type on to a variable. This can be done with casting.
    # ex.
        # print("My age is " + age)

    # Some functions that can be used:
        # 1. int() - converts the value into an integer value
        # 2. float() - converts the value into a float value
        # 3. str() - converts the value into strings

print("My age is " + str(age))
print(int("11111"))
print(float(10))

# F-strings
print(f"Hi! The Name's {full_name} and my age's {age}")

# Operations
print(1 + 10) #addition
print(15 - 8) #subtraction
print(1 * 1) #multiplication
print(12 / 6) #floating division
print(12 // 6) #division
print(2 ** 6) #exponent
print(18 % 4) #remainder

# Assignment Operators
num1 = 3
# Traditional Operators: -=   *=   /=   %= 
num1 += 4 #num1 = num1 + 4

print(num1)

# Logical operators
print(True and False)
print(not False)
print(False or True)

# Comparison operators
print(1 != 1)
print(1 == 1)
print(1 > 2)
print(2 < 1)
print(1 <= 1)
print(1 >= 1)