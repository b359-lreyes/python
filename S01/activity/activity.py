name = "Luke Victor Reyes"
age = 22
occupation = "student"
movie = "It"
rating = 60.99
print(f"I am {name}, and I am {age} years old. I work as a {occupation}, and my rating for the movie: {movie} is {rating}%")

num1, num2, num3 = 1, 2, 3
print(num1 * num2)
print(num1 < num3)
print(num3 + num2)