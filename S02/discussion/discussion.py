# Input
# input() : seeks to gather data from user input
#  \n stands for line break
username = input("Please enter your name: \n")
print(f"Hi {username}! Welcome")

num1 = int(input("Enter 1st number: \n"))
num2 = int(input("Enter 2nd number \n"))
print(f"The sum of num1 and num2: {num1 + num2}")

print("=============================================================")

# if-else Statements
# Control Structures
    # 1. Selection
        # allow the program to choose among choices and run specific codes depending on the choice taken
    # 2. Repetition
        # allow the program to repeat certain blocks of code given a starting condition and terminating condition
test_num = 75
if test_num >= 60:
    print("passed")
else:
    print("failed")
    
#If Else chains
# elif : shorthand for "else if"
test_num2 = int(input("Please enter the 2nd test number \n"))
if test_num2 > 0:
    print("Positive")
elif test_num2 == 0:
    print("zero")
else:
    print("negative")

print("=============================================================")

# Mini Exercise 1:
# Create an if-elese statement that determines if a number is divisible by 3, 5 or both.
    # if the number is divisible by 3, print "The number is divisible by 3"
    # if the number is divisible by 5, print "The number is divisible by 5"
    # if the number is divisible by 3 and 5, print "The number is divisible by both 3 and 5"
    # if the number is not divisible by any, print "The number is not divisible by 3 nor 5"
div_num = int(input("number: "))
if div_num % 3 == 0:
    print("The number is divisible by 3")
elif div_num % 5 == 0:
    print("The number is divisible by 5")
elif div_num % 3 == 0 and div_num % 5 == 0:
    print("The number is divisible by both 3 and 5")
else:
    print("The number is not divisible by 3 nor 5")

print("=============================================================")

# Loops
# While : Used to execute a set of statements as long as the condition is true
i = 1
while i <= 5:
    print(f"The current count: {i}")
    i += 1

print("=============================================================")

# range() : methoid that return the sequence of the given number
    # Syntax:
    # range(stop)
    # range(start, stop)
    # range(start, stop, step)
# the range() function defaults to 0 as a starting value. As such, this prints the values from 0-5
for x in range(6):
    print(f"the current value is {x}")

# This prints the value from 6-9
for x in range(6, 10):
    print(f"The current value is {x}")

# A 3rd argument can be added to specify the increment of the loop.
for x in range (6, 20, 2):
    print(f"The current value is {x}")

print("=============================================================")

#Mini Exercise 2:
#Create a loop to count and display the number of even numbers between 1 and 20.
    # Print "The number of even number between 1 and 20 is: <a total number of even numbers>"
count = 0
for num in range(1, 21):
    if num % 2 == 0 :
        count += 1
print("The number of even numbers between 1 and 20 is: ", count)

# Mini Exercise:
# Write a program that takes an integer input from the user and displays the multiplication table for that number. From 1 - 10
    # Ex.
        # 5 x 1 = 5
        # 5 x 2 = 10
        # 5 x 3 = 15
        # 5 x 4 = 20
        # 5 x 5 = 25
num = int(input("Enter any number: "))

for i in range(1, 11):
    result = num * i
    print(num, "x", i, "=", result)

print("=============================================================")

# Break Statement - used to stop loops
j = 1

while j < 6:
    print(j)
    if j == 3:
        break
    j += 1

print("=============================================================")

# Continue Statement - returns the control to the beginning of the while loop and continue with the next iteration
k = 1
while k < 6:
    k += 1
    if k == 3:
        continue
    print(k)