# Activity:
    # 1. Accept a year input from the user and determine if it is a leap year or not.
    # 2. Accept two numbers (row and col) from the user and create a grid of asterisks using the two numbers (row and col).

year = int(input("Please input a year: "))

if(year % 4 == 0 and year % 100 != 0) or (year % 400 == 0):
    print(year, " is a leap year.")
else:
    print(year, " is not a leap year.")

print("\n")

row = int(input("Enter number of rows: "))
col = int(input("Enter number of columns: "))
for i in range(row):
    for j in range(col):
        print("*", end=" ")
    print()