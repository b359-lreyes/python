# Specification
#  1. Create an abstract class called Animal that has the following abstract methods
#     Abstract Methods: eat(food), make_sound()
#  2. Create two classes that implements the Animal class called Cat and Dog with each of the following properties and methods:
#     Properties:
#        name, breed, age
#    Methods:
#       getters and setters, implementation of abstract methods, call()

from abc import ABC, abstractclassmethod

class Animal(ABC):
    @abstractclassmethod
    def eat(self, food):
        pass
    def make_sound(self):
        pass

class Dog(Animal):
    def __init__(self, name, breed, age):
        self.name = name
        self.breed = breed
        self.age = age

    def eat(self, food):
        print(f"{self.name} will have {food} for lunch")

    def make_sound(self):
        print(f"Bark! Woof! Arf! Sounds like my {self.name}")

    def call(self):
        print(f"Here, {self.name}!")
        
class Cat(Animal):
    def __init__(self, name, breed, age):
        self.name = name
        self.breed = breed
        self.age = age

    def eat(self, food):
        print(f"Serve me {food}")

    def make_sound(self):
        print(f"Miaow! Nyaw! Nyaaaa! Sounds like my {self.name}")

    def call(self):
        print(f"{self.name}, come on!")

# Test Cases:
dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()






# or







# Specification
# 1. Create an abstract class called Animal that has the following abstract methods
# Abstract Methods: eat(food), make_sound()
# 2. Create two classes that implements the Animal class called Cat and Dog with each of the following properties and methods:
# Properties:
#   name, breed, age
# Methods:
#   getters and setters, implementation of abstract methods, call()

# Solution:
from abc import ABC, abstractclassmethod

class Animal(ABC):
    @abstractclassmethod
    def eat(self, food):
        # The pass keyword denotes that the method doesn't do anything.
        pass

    @abstractclassmethod
    def make_sound(self):
        pass    

class Dog(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age
    
    def get_name(self):
        return self._name

    def get_breed(self):
        return self._breed
    
    def get_age(self):
        return self._age

    def set_name(self, name):
        self._name = name
    
    def set_breed(self, breed):
        self._breed = breed

    def set_age(self, age):
        self._age = age

    def eat(self, food):
        print(f"Eaten {food}")

    def make_sound(self):
        print(f"Bark! Woof! Arf!")

    def call(self):
        print(f"Here {self._name}!")

class Cat(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age

    def get_name(self):
        return self._name

    def get_breed(self):
        return self._breed

    def get_age(self):
        return self._age

    def set_name(self, name):
        self._name = name

    def set_breed(self, breed):
        self._breed = breed

    def set_age(self, age):
        self._age = age

    def eat(self, food):
        print(f"Serve me {food}")

    def make_sound(self):
        print(f"Miaow! Nyaw! Nyaaaaa!")

    def call(self):
        print(f"{self._name}, come on!")

# Test Cases:
dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()

